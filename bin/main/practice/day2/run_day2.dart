import 'dart:io';
import 'package:dart_tutorial/functions/practice/day2/no_1.dart' as dt_no_1;
import 'package:dart_tutorial/functions/practice/day2/no_2.dart' as dt_no_2;
import 'package:dart_tutorial/functions/practice/day2/no_3.dart' as dt_no_3;
import 'package:dart_tutorial/functions/practice/day2/no_4.dart' as dt_no_4;
import 'package:dart_tutorial/functions/practice/day2/no_5.dart' as dt_no_5;
import 'package:dart_tutorial/functions/practice/day2/no_6.dart' as dt_no_6;
import 'package:dart_tutorial/functions/practice/day2/no_7.dart' as dt_no_7;
import 'package:dart_tutorial/functions/practice/day2/no_8.dart' as dt_no_8;
import 'package:dart_tutorial/functions/practice/day2/no_9.dart' as dt_no_9;


void main(List<String> arguments){
  stdout.write("Name \t: ");
  String? name = stdin.readLineSync();
  stdout.write("Job \t: ");
  String? job = stdin.readLineSync();
  stdout.write("\n");

  print("No 1. ${dt_no_1.question()} => ");
  stdout.write("      Value \t: ");
  String? valueToCheckOddOrEven = stdin.readLineSync();
  print("      Result \t: ${dt_no_1.answer(valueToCheckOddOrEven!)}");

  print("No 2. ${dt_no_2.question()} => ");
  stdout.write("      Value \t: ");
  String? valueToCheckVowelOrConsonant = stdin.readLineSync();
  print("      Result \t: ${dt_no_2.answer(valueToCheckVowelOrConsonant!)}");

  print("No 3. ${dt_no_3.question()} => ");
  stdout.write("      Value \t: ");
  String? valueToCheckPositiveOrNegativeOrZero = stdin.readLineSync();
  print("      Result \t: ${dt_no_3.answer(valueToCheckPositiveOrNegativeOrZero!)}");

  print("No 4. ${dt_no_4.question()} => ");
  stdout.write("      Value \t: ");
  String? valueToPrint100Times = stdin.readLineSync();
  print("      Result \t: \n${dt_no_4.answer(valueToPrint100Times!)}");

  print("No 5. ${dt_no_5.question()} => ");
  stdout.write("      Value 1 \t: ");
  String? value1ToSum = stdin.readLineSync();
  stdout.write("      Value 2 \t: ");
  String? value2ToSum = stdin.readLineSync();
  print("      Result  \t: ${dt_no_5.answer(value1ToSum!,value2ToSum!)}");

  print("No 6. ${dt_no_6.question()} => ");
  stdout.write("      Value 1 \t: ");
  String? valueBuffer = stdin.readLineSync();
  stdout.write("      Value 2 \t: ");
  String? valueMultiplier = stdin.readLineSync();
  print("      Result \t: \n${dt_no_6.answer(valueBuffer!, valueMultiplier!)}");

  print("No 7. ${dt_no_7.question()} => ");
  stdout.write("      Value 1 \t: ");
  String? valueBufferNew = stdin.readLineSync();
  stdout.write("      Value 2 \t: ");
  String? valueMultiplierNew = stdin.readLineSync();
  print("      Result \t: \n${dt_no_7.answer(valueBufferNew!, valueMultiplierNew!)}");

  print("No 8. ${dt_no_8.question()} => ");
  stdout.write("      Value 1 \t: ");
  String? value1SimpleOperation = stdin.readLineSync();
  stdout.write("      Value 2 \t: ");
  String? value2SimpleOperation = stdin.readLineSync();
  print("      Result \t: \n${dt_no_8.answer(value1SimpleOperation!, value2SimpleOperation!)}");

  print("No 9. ${dt_no_9.question()} => ");
  stdout.write("      Value 1 \t: ");
  String? valueStart = stdin.readLineSync();
  stdout.write("      Value 2 \t: ");
  String? valueEnd = stdin.readLineSync();
  print("      Result \t: \n${dt_no_9.answer(valueStart!, valueEnd!)}");
}
