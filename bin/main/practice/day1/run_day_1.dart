import 'dart:io';
import 'package:dart_tutorial/functions/practice/day1/no_1.dart' as dt_no_1;
import 'package:dart_tutorial/functions/practice/day1/no_2.dart' as dt_no_2;
import 'package:dart_tutorial/functions/practice/day1/no_3.dart' as dt_no_3;
import 'package:dart_tutorial/functions/practice/day1/no_4.dart' as dt_no_4;
import 'package:dart_tutorial/functions/practice/day1/no_5.dart' as dt_no_5;
import 'package:dart_tutorial/functions/practice/day1/no_6.dart' as dt_no_6;
import 'package:dart_tutorial/functions/practice/day1/no_7.dart' as dt_no_7;
import 'package:dart_tutorial/functions/practice/day1/no_8.dart' as dt_no_8;
import 'package:dart_tutorial/functions/practice/day1/no_9.dart' as dt_no_9;
import 'package:dart_tutorial/functions/practice/day1/no_10.dart' as dt_no_10;
import 'package:dart_tutorial/functions/practice/day1/no_11.dart' as dt_no_11;
import 'package:dart_tutorial/functions/practice/day1/no_12.dart' as dt_no_12;

void main(List<String> arguments){
  stdout.write("Name \t: ");
  String? name = stdin.readLineSync();
  stdout.write("Job \t: ");
  String? job = stdin.readLineSync();
  stdout.write("\n");

  print("No 1. ${dt_no_1.question()} => ${dt_no_1.answer(name!)}");
  print("No 2. ${dt_no_2.question()} => ${dt_no_2.answer("John Doe")}");
  print("No 3. ${dt_no_3.question()} => ${dt_no_3.answer()}");
  print("No 4. ${dt_no_4.question()} => ${dt_no_4.answer(10,78,34)}");

  print("No 5. ${dt_no_5.question()} => ");
  stdout.write("      Value \t: ");
  String? value = stdin.readLineSync();
  print("      Result \t: ${dt_no_5.answer(num.parse(value!))}");

  print("No 6. ${dt_no_6.question()} => ");
  stdout.write("      First Name \t: ");
  String? firstName = stdin.readLineSync();
  stdout.write("      Last Name \t: ");
  String? lastName = stdin.readLineSync();
  print("      Fullname \t: ${dt_no_6.answer(firstName!,lastName!)}");

  print("No 7. ${dt_no_7.question()} => ");
  stdout.write("      Value 1 \t: ");
  String? value1 = stdin.readLineSync();
  stdout.write("      Value 2 \t: ");
  String? value2 = stdin.readLineSync();
  print("      Result \t: ${dt_no_7.answer(int.parse(value1!),int.parse(value2!))}");

  print("No 8. ${dt_no_8.question()} => ");
  stdout.write("      Value 1 \t: ");
  String? initValue1 = stdin.readLineSync();
  stdout.write("      Value 2 \t: ");
  String? initValue2 = stdin.readLineSync();
  print("      Result \t: ${dt_no_8.answer(int.parse(initValue1!),int.parse(initValue2!))}");

  print("No 9. ${dt_no_9.question()} => ");
  stdout.write("      Value \t: ");
  String? sentence = stdin.readLineSync();
  print("      Result \t: ${dt_no_9.answer(sentence!)}");

  print("No 10. ${dt_no_10.question()} => ");
  stdout.write("      Value \t: ");
  String? sentences = stdin.readLineSync();
  print("      Result \t: ${dt_no_10.answer(sentences!)}");

  print("No 11. ${dt_no_11.question()} => ");
  stdout.write("      Bill \t: ");
  String? bill = stdin.readLineSync();
  stdout.write("      Total Person \t: ");
  String? totalPerson = stdin.readLineSync();
  print("      Result \t: ${dt_no_11.answer(double.parse(bill!),int.parse(totalPerson!))}");

  print("No 12. ${dt_no_12.question()} => ${dt_no_12.answer(25,40)}");
}


