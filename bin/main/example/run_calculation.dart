import 'package:dart_tutorial/functions/example/calculation_basic.dart' as dart_tutorial_calculation;

void main(List<String> arguments) {
  int num1 = 9;
  int num2 = 6;
  print('Hasil Jumlah: ${dart_tutorial_calculation.add(num1, num2)}!');
  print('Hasil Kurang: ${dart_tutorial_calculation.sub(num1, num2)}!');
  print('Hasil Kali: ${dart_tutorial_calculation.mul(num1, num2)}!');
  print('Hasil Bagi: ${dart_tutorial_calculation.div(num1, num2)}!');
}
