class customException implements Exception {
  String wrongInputMustBeInteger(String value) {
    return "ERROR!!! Your Input Value Have a Wrong Type \n \t\t\t\t  Your Input : $value \n \t\t\t\t  Reason \t : Input Must Be Integer";
  }

  String wrongInputLengthMusBe1(String value) {
    int lengthValue = value.length;
    return "ERROR!!! Your Input Value Have Length Not Equals 1 \n \t\t\t\t  Your Input : $value \n \t\t\t\t  Length \t : $lengthValue \n \t\t\t\t  Reason \t : Input Must Be Have Length 1";
  }

  String wrongInputNotExistInAlphabet(String value) {
    return "ERROR!!! Your Input Value Not Exist In Alphabet \n \t\t\t\t  Your Input : $value \n \t\t\t\t  Reason \t : Input Must Be Exist In Alphabet";
  }

  String wrongInputCanNotBeNull(String value) {
    return "ERROR!!! Your Input Value Have a Wrong Format \n \t\t\t\t  Your Input : $value \n \t\t\t\t  Reason \t : Input Must Be Not NULL";
  }

  String wrongInputCanNotBeEmpty(String value) {
    return "ERROR!!! Your Input Value Have a Wrong Format \n \t\t\t\t  Your Input : $value \n \t\t\t\t  Reason \t : Input Must Be Not Empty String";
  }

  String wrongInputCanNotBeFullWhitespace(String value) {
    return "ERROR!!! Your Input Value Have a Wrong Format \n \t\t\t\t  Your Input : $value \n \t\t\t\t  Reason \t : Input Must Be Not Full Whitespace";
  }

  String wrongInputNotPositiveNumber(String value) {
    return "ERROR!!! Your Input Value Have a Wrong Type \n \t\t\t\t  Your Input : $value \n \t\t\t\t  Reason \t : Input Must Be Integer Positive";
  }

  String wrongInputMustBeNumber(String value) {
    return "ERROR!!! Your Input Value Have a Wrong Type \n \t\t\t\t  Your Input : $value \n \t\t\t\t  Reason \t : Input Must Be Number";
  }

  String wrongInputDividerCannotBeZero(String value) {
    return "ERROR!!! Your Input Divider Value is Wrong \n \t\t\t\t  Your Input : $value \n \t\t\t\t  Reason \t : Input  Divider Must Be Number Not Zero";
  }

  String wrongInputValue2MustBeHigherThanValue1(String value1, String value2) {
    return "ERROR!!! Your Input Value is Wrong \n \t\t\t\t  Your Input 1: $value1 \n \t\t\t\t  Your Input 2: $value2 \n \t\t\t\t  Reason \t : Input  Value 2 Must Be Higher Than Value 1";
  }
}
