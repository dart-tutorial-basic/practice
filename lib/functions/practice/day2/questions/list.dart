List<String> listQuestion = [
  "Write a dart program to check if the number is odd or even.",
  "Write a dart program to check whether a character is a vowel or consonant.",
  "Write a dart program to check whether a number is positive, negative, or zero.",
  "Write a dart program to print your name 100 times.",
  "Write a dart program to calculate the sum of natural numbers.",
  "Write a dart program to generate multiplication tables of 5.",
  "Write a dart program to generate multiplication tables of 1-9.",
  "Write a dart program to create a simple calculator that performs addition, subtraction, multiplication, and division.",
  "Write a dart program to print 1 to 100 but not 41."
];
