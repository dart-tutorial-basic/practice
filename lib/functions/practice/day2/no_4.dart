import 'package:dart_tutorial/functions/practice/day2/questions/list.dart'
    as ld;
import 'package:dart_tutorial/functions/response/error/customException.dart'
    as ce;

String question() {
  return ld.listQuestion[3];
}

String answer(String value) {
  if(checkInput(value)=="OK"){
    String value100Times = "";
    for (int i = 0; i<loopInput100Times(value).length ; i++){
      value100Times += "             \t  ${loopInput100Times(value)[i]}\n";
    }
    return value100Times;

  }else{
    return checkInput(value);
  }
}

String checkInput(String input) {
  if (input.replaceAll(" ", "").length == 0) {
    return ce.customException().wrongInputCanNotBeFullWhitespace(input);
  } else if (input.length==0) {
    return ce.customException().wrongInputCanNotBeEmpty(input);
  } else if (input == null) {
    return ce.customException().wrongInputCanNotBeNull(input);
  }else{
    return "OK";
  }
}

List<String> loopInput100Times(String input){
  List<String> input100Times = [];
  for(int i = 0; i<100; i++){
    input100Times.add(input);
  }
  return input100Times;
}
