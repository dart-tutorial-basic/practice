import 'package:dart_tutorial/functions/practice/day2/questions/list.dart'
    as ld;
import 'package:dart_tutorial/functions/response/error/customException.dart'
    as ce;

String question() {
  return ld.listQuestion[8];
}

String answer(String value1, String value2) {
  int value1ToInt = 0;
  int value2ToInt = 0;
  String loop1To100 = "";

  try {
    value1ToInt += int.parse(value1);
  } catch (ex) {
    // throw ce.customException().wrongInputMustBeInteger(value);
    return ce.customException().wrongInputMustBeInteger(value1);
  }

  try {
    value2ToInt += int.parse(value2);
  } catch (ex) {
    // throw ce.customException().wrongInputMustBeInteger(value);
    return ce.customException().wrongInputMustBeInteger(value2);
  }

  if (checkInputMustBeIntegerPositif(value1ToInt, value2ToInt) != "OK") {
    return checkInputMustBeIntegerPositif(value1ToInt, value2ToInt);
  }

  if (checkInput2MustBeHigherThanInput1(value1ToInt, value2ToInt) != "OK") {
    return checkInput2MustBeHigherThanInput1(value1ToInt, value2ToInt);
  }

  for (int i = value1ToInt; i <= value2ToInt; i++) {
    if(i==41){
      continue;
    }
    loop1To100 +=
        "             \t  Data ke - $i => $i\n";
  }
  return loop1To100;
}

String checkInputMustBeIntegerPositif(int buffer, int multiplier) {
  if (buffer <= 0) {
    return ce.customException().wrongInputNotPositiveNumber(buffer.toString());
  } else if (multiplier <= 0) {
    return ce
        .customException()
        .wrongInputNotPositiveNumber(multiplier.toString());
  }

  return "OK";
}

String checkInput2MustBeHigherThanInput1(int value1, int value2) {
  if(value2<=value1){
    return ce.customException().wrongInputValue2MustBeHigherThanValue1(value1.toString(), value2.toString());
  }

  return "OK";
}
