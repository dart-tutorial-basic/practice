import 'package:dart_tutorial/functions/practice/day2/questions/list.dart'
    as ld;
import 'package:dart_tutorial/functions/response/error/customException.dart'
    as ce;

String question() {
  return ld.listQuestion[4];
}

String answer(String value1, String value2) {
  int value1ToInt = 0;
  int value2ToInt = 0;

  try {
     value1ToInt = int.parse(value1);
  } catch (ex) {
    // throw ce.customException().wrongInputMustBeInteger(value);
    return ce.customException().wrongInputMustBeInteger(value1);
  }

  try {
    value2ToInt = int.parse(value2);
  } catch (ex) {
    // throw ce.customException().wrongInputMustBeInteger(value);
    return ce.customException().wrongInputMustBeInteger(value2);
  }

  return addition(value1ToInt, value2ToInt);

}

String addition(int value1, int value2) {
  if (value1 <= 0) {
    return ce.customException().wrongInputNotPositiveNumber(value1.toString());
  } else if (value2 <= 0) {
    return ce.customException().wrongInputNotPositiveNumber(value2.toString());
  }
  return (value1 + value2).toString();
}
