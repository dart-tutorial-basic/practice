import 'package:dart_tutorial/functions/practice/day2/questions/list.dart'
    as ld;
import 'package:dart_tutorial/functions/response/error/customException.dart'
    as ce;

String question() {
  return ld.listQuestion[2];
}

String answer(String value) {
  try {
    return checkInput(int.parse(value));
  } catch (ex) {
    return ce.customException().wrongInputMustBeInteger(value);
  }
}

String checkInput(int input) {
  if (input > 0) {
    return "$input is Positive";
  } else if (input < 0) {
    return "$input is Negative";
  } else {
    return "$input is Zero";
  }
}
