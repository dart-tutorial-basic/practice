import 'package:dart_tutorial/functions/practice/day2/questions/list.dart'
    as ld;
import 'package:dart_tutorial/functions/response/error/customException.dart'
    as ce;
import 'package:dart_tutorial/functions/util/listVowelConsonant.dart'
    as lvc;

String question() {
  return ld.listQuestion[1];
}

String answer(String value) {
  if(checkLengthInput(value).length==1){
    if(lvc.alphabet().contains(checkInputIsAlphabetOrNot(value))){
      return checkInputIsVowelOrConsonant(value);
    }else{
      return checkInputIsAlphabetOrNot(value);
    }
  }else{
    return checkLengthInput(value);
  }
}

String checkLengthInput(String input) {
  if (input.length == 1) {
    return input.toLowerCase();
  } else {
    return ce.customException().wrongInputLengthMusBe1(input);
  }
}

String checkInputIsAlphabetOrNot(String input){
   if(lvc.alphabet().contains(input.toLowerCase())){
     return input.toLowerCase();
   }else{
     return ce.customException().wrongInputNotExistInAlphabet(input);
   }
}

String checkInputIsVowelOrConsonant(String input) {
  if(lvc.vowel().contains(input.toLowerCase())){
    return "$input is VOWEL";
  }else if(lvc.consonant().contains(input.toLowerCase())){
    return "$input is CONSONANT";
  }else{
    return "Null";
  }
}
