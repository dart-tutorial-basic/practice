import 'package:dart_tutorial/functions/practice/day2/questions/list.dart'
    as ld;
import 'package:dart_tutorial/functions/response/error/customException.dart'
    as ce;

String question() {
  return ld.listQuestion[7];
}

String answer(String value1, String value2) {
  num value1ToInt = 0;
  num value2ToInt = 0;
  String resultSimpleOperation = "";

  try {
    value1ToInt += num.parse(value1);
  } catch (ex) {
    // throw ce.customException().wrongInputMustBeInteger(value);
    return ce.customException().wrongInputMustBeNumber(value1);
  }

  try {
    value2ToInt += num.parse(value2);
  } catch (ex) {
    // throw ce.customException().wrongInputMustBeInteger(value);
    return ce.customException().wrongInputMustBeNumber(value2);
  }

  for (int i = 0; i < resultOperation(value1ToInt, value2ToInt).length; i++) {
    resultSimpleOperation +=
        "             \t  ${resultOperation(value1ToInt, value2ToInt)[i]}\n";
  }
  return resultSimpleOperation;
}

List<String> resultOperation(num value1, num value2){
   List<String> valueResult = [];
   valueResult.add(addition(value1, value2));
   valueResult.add(substraction(value1, value2));
   valueResult.add(multiplication(value1, value2));
   valueResult.add(division(value1, value2));
   return valueResult;

}

String addition(num value1, num value2){
  return "$value1 + $value2 = ${value1+value2}";

}

String substraction(num value1, num value2){
  return "$value1 - $value2 = ${value1-value2}";

}

String multiplication(num value1, num value2){
  return "$value1 * $value2 = ${value1*value2}";

}

String division(num value1, num value2){
  if(value2 == 0){
    return ce.customException().wrongInputDividerCannotBeZero(value2.toString());
  }
  return "$value1 / $value2 = ${value1/value2}";
}
