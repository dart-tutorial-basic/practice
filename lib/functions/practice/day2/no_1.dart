import 'package:dart_tutorial/functions/practice/day2/questions/list.dart' as ld;
import 'package:dart_tutorial/functions/response/error/customException.dart' as ce;

String question(){
  return ld.listQuestion[0];
}

String answer(String value){
  try{
    return checkNumberIsOddOrEven(int.parse(value));
  }catch(ex){
    // throw ce.customException().wrongInputMustBeInteger(value);
    return ce.customException().wrongInputMustBeInteger(value);
  }

}

String checkNumberIsOddOrEven(int value){
  return (value%2==0) ? "$value is even" : "$value is odd";
}
