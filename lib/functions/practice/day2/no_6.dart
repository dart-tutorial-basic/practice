import 'package:dart_tutorial/functions/practice/day2/questions/list.dart'
    as ld;
import 'package:dart_tutorial/functions/response/error/customException.dart'
    as ce;

String question() {
  return ld.listQuestion[5];
}

String answer(String value1, String value2) {
  int value1ToInt = 0;
  int value2ToInt = 0;
  String tableGenerateMultiplication = "";

  try {
    value1ToInt += int.parse(value1);
  } catch (ex) {
    // throw ce.customException().wrongInputMustBeInteger(value);
    return ce.customException().wrongInputMustBeInteger(value1);
  }

  try {
    value2ToInt += int.parse(value2);
  } catch (ex) {
    // throw ce.customException().wrongInputMustBeInteger(value);
    return ce.customException().wrongInputMustBeInteger(value2);
  }

  if (checkInputMustBeINtegerPositif(value1ToInt, value2ToInt) != "OK") {
    return checkInputMustBeINtegerPositif(value1ToInt, value2ToInt);
  }

  for (int i = 0; i < multiplication(value1ToInt, value2ToInt).length; i++) {
    tableGenerateMultiplication +=
        "             \t  ${multiplication(value1ToInt, value2ToInt)[i]}\n";
  }
  return tableGenerateMultiplication;
}

String checkInputMustBeINtegerPositif(int buffer, int multiplier) {
  if (buffer <= 0) {
    return ce.customException().wrongInputNotPositiveNumber(buffer.toString());
  } else if (multiplier <= 0) {
    return ce
        .customException()
        .wrongInputNotPositiveNumber(multiplier.toString());
  }

  return "OK";
}

List<String> multiplication(int buffer, int multiplier) {
  List<String> resultMultiplication = [];
  for (int i = 0; i <= buffer; i++) {
    resultMultiplication.add("$i * $multiplier = ${i * multiplier}");
  }
  return resultMultiplication;
}
