import "package:dart_tutorial/functions/practice/day1/questions/list.dart" as lists;

String question(){
  return lists.listQuestion[5];
}

String answer(String firstName, String lastName){
  return fullName(firstName, lastName);
}

String fullName(String firstName, String lastName){
  return "$firstName $lastName";
}
