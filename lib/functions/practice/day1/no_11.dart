import "package:dart_tutorial/functions/practice/day1/questions/list.dart" as lists;

String question(){
  return lists.listQuestion[10];
}

String answer(double bill, int totalPerson){
  return "\$${splitBill(bill, totalPerson)}";
}

double splitBill(double bill, int totalPerson){
  return bill / totalPerson;
}
