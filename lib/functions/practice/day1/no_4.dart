import "package:dart_tutorial/functions/practice/day1/questions/list.dart" as lists;

String question(){
  return lists.listQuestion[3];
}

String answer(int principal, int time, int ratio){
  return countSimpleInterest(principal, time, ratio).toString();
}


double countSimpleInterest(int principal, int time, int ratio){
  return (principal*time*ratio)/100;
}
