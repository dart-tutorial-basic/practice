import "package:dart_tutorial/functions/practice/day1/questions/list.dart" as lists;
import "dart:math";

String question(){
  return lists.listQuestion[4];
}

String answer(num value){
  return valueSquareRoot(value).toString();
}

num valueSquareRoot(num value){
  return pow(value, 2);
}
