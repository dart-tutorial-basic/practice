import "package:dart_tutorial/functions/practice/day1/questions/list.dart" as lists;

String question(){
  return lists.listQuestion[9];
}

int answer(String sentence){
  return convertStringToInt(sentence);
}

int convertStringToInt(String sentenceToInt){
  return int.parse(sentenceToInt);
}
