import "package:dart_tutorial/functions/practice/day1/questions/list.dart" as lists;

String question(){
  return lists.listQuestion[6];
}

String answer(int value1, int value2){
  return "Quotient => ${quotient(value1, value2)} and Remainder => ${remainder(value1, value2)}";
}

int quotient(int value1, int value2){
  return value1 ~/ value2;
}

int remainder(int value1, int value2){
  return value1 % value2;
}
