import "package:dart_tutorial/functions/practice/day1/questions/list.dart" as lists;

String question(){
  return lists.listQuestion[1];
}

String answer(String name){
  return "${prefixWithoutQuote()}${names(name)} and ${prefixWithQuote()}${names(name)}";
}


String prefixWithoutQuote(){
  return "Hello I am ";
}

String prefixWithQuote(){
  return "Hello I'm ";
}

String names(String name){
  return "\"$name\"";
}


