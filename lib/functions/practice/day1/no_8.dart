import "package:dart_tutorial/functions/practice/day1/questions/list.dart" as lists;

String question(){
  return lists.listQuestion[7];
}

String answer(num value1, num value2){
  return swapNum(value1, value2);
}

String swapNum(num value1, num value2){
  return "$value2,$value1";
}
