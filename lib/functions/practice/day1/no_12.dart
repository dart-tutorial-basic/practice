import "package:dart_tutorial/functions/practice/day1/questions/list.dart" as lists;

String question(){
  return lists.listQuestion[11];
}

String answer(int distance, int speed){
return timeTakenInMinutes(distance, speed).toString();
}

double timeTakenInMinutes(int distance, int speed){
  return timeTakenInHour(distance, speed)/60;
}

double timeTakenInHour(int distance, int speed){
  return distance / speed;
}
