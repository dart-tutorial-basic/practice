import "package:dart_tutorial/functions/practice/day1/questions/list.dart" as lists;

String question(){
  return lists.listQuestion[8];
}

String answer(String sentence){
  return removeAllWhiteSpace(sentence);
}

String removeAllWhiteSpace(String sentence){
  return sentence.replaceAll(" ", "");
}
