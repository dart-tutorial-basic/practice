List<String> alphabet() {
  List<String> alphabet = [
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'l',
    'm',
    'n',
    'o',
    'p',
    'q',
    'r',
    's',
    't',
    'u',
    'v',
    'w',
    'x',
    'y',
    'z'
  ];
  return alphabet;
}

List<String> vowel() {
  List<String> vowel = [];
  List<String> alphabets = alphabet();
  for (int i = 0; i < alphabets.length; i++) {
    if (alphabets[i] == 'a' ||
        alphabets[i] == 'i' ||
        alphabets[i] == 'u' ||
        alphabets[i] == 'e' ||
        alphabets[i] == 'o') {
      vowel.add(alphabets[i]);
    }
  }
  return vowel;
}

List<String> consonant() {
  List<String> consonant = [];
  List<String> alphabets = alphabet();
  for (int i = 0; i < alphabets.length; i++) {
    if (alphabets[i] != 'a' ||
        alphabets[i] != 'i' ||
        alphabets[i] != 'u' ||
        alphabets[i] != 'e' ||
        alphabets[i] != 'o') {
      consonant.add(alphabets[i]);
    }
  }
  return consonant;
}
